i3

Dependencies:
- dmenu
- libmpdclient (for status.Mpd)
- python 2 or 3 (for i3status.py)

What I don't like:

- No window title in i3bar possible
- i3bar doesn't support colors (in Ubuntu version)?
- Scripting of i3bar circumstantial

What I like:

- Small (package size)
- Fast (written in C)
- Sane configuration file
- i3bar supports system tray
- Though circumstantial, scripting i3bar allows for energy-efficient updating
  because the script (here: i3status.py) will only be called once and updates
  can be performed as often as desired.
