#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set sw=4 et tw=79:

from __future__ import unicode_literals
from __future__ import print_function

import time
import sys
import json
import signal
import functools
import importlib
import argparse
import collections


def cached_result(refresh_timeout):
    """
    Returns a decorator function for parameter-less functions caching returned
    values which are refreshed every refresh_timeout seconds.
    """

    def real_cached_result(func):
        @functools.wraps(func)
        def inner():
            curr_time = time.time()

            if not hasattr(inner, "last_refresh") or \
                curr_time - inner.last_refresh > refresh_timeout:
                    inner.last_refresh = curr_time
                    inner.cache = func()

            return inner.cache

        result = inner

        return result

    return real_cached_result



ModuleInfo = collections.namedtuple("ModuleInfo", ["func" , "color"])


def parse_config(args):
    """
    Tries parsing the json file args.config_file and uses the information to
    return a list of ModuleInfo objects.

    If the parsing fails, an error is written to stderr and an empty list is
    returned.

    args.color must be a string defining the default color to be used for the
    modules.

    args.refresh must be an int defining the default refresh to be used for the
    modules.
    """

    if not args.config_file:
        return []

    module_infos = []
    config = None

    # Read config file

    try:
        with open(args.config_file, "r") as file:
            # OrderedDict is important to keep the order of the module entries
            # as they are specified in the config file.
            config = json.load(file, object_pairs_hook=collections.OrderedDict)
    except ValueError:
        sys.stderr.write("{}: Invalid config file.".format(sys.argv[0]))
        return module_infos
    except IOError:
        sys.stderr.write("{}: Config file doesn't exist.".format(sys.argv[0]))
        return module_infos

    for module_name, module_config in config.items():
        full_module_name = "status.{}".format(module_name)

        # Load module

        module = None

        try:
            module = importlib.import_module(full_module_name)
        except ImportError:
            sys.stderr.write("{}: No such module: {}. Skipping\n".format \
                ( sys.argv[0]
                , full_module_name
                ))
            continue

        # Instantiate object

        module_object = None

        try:
            if "config" in module_config:
                module_object = getattr(module, module_name)(
                    module_config["config"])
            else:
                module_object = getattr(module, module_name)()
        except AttributeError:
            sys.stderr.write("{}: No constructor named {}() in module: {}. "
                "Skipping\n".format \
                    ( sys.argv[0]
                    , module_name
                    , full_module_name
                    ))
            continue

        # Check for "status" function

        try:
            getattr(module_object, "status")()
        except AttributeError:
            sys.stderr.write("{}: {} class has no \"status\" function. "
                "Skipping\n".format \
                    ( sys.argv[0]
                    , module_name
                    ))
            continue
        except TypeError:
            sys.stderr.write("{}: \"status\" in class {} is no callable. "
                "Skipping\n".format \
                    ( sys.argv[0]
                    , module_name
                    ))
            continue

        # Check for color
        color = module_config.get("color", args.color)

        # Check for refresh
        refresh = module_config.get("refresh", args.refresh)

        # Replace "status" function in object
        func = cached_result(refresh)(getattr(module_object, "status"))

        module_infos.append(
            ModuleInfo(func=func, color=color)
            )

    return module_infos


desc_epilog = \
"""
The config file must be a properly formatted JSON file containing a map whose
keys are the names of modules in the "status" package. Each module must feature
a self-named constructor and a "status" function returning a string.

The value associated with a module name must again be a map. This map may have
the following keys:

- "color"
  A string defining the color of the output.

- "refresh"
  An integer defining the number of seconds after which the respective output
  should be updated.

- "config"
  Arbitrary data that will be passed to the module's constructor as-is.

An example config file may look like this:

{ "Mpd" :
    { "color"   : "#8ac6f2"
    , "refresh" : 3
    }
, "Battery" :
    { "color"   : "#f6f3e8"
    , "refresh" : 15
    , "config"  :
        { "name" : "BAT1"
        }
    }
, "Time" :
    { "color"   : "#8ac6f2"
    , "refresh" : 1
    }
}
"""


def main():
    parser = argparse.ArgumentParser(
        description="Print status information for i3bar.",
        epilog=desc_epilog,
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-c", "--config", dest="config_file", default=None,
        help="Config file")

    parser.add_argument("-C", "--color", dest="color", default="#FFFFFF",
        help="Default color")

    parser.add_argument("-T", "--refresh", dest="refresh", default=5,
        help="Default refresh", type=int)

    args = parser.parse_args()

    module_infos = parse_config(args)

    sys.stdout.write(json.dumps({ "version": 1 }))
    sys.stdout.write("\n[\n")
    sys.stdout.write("[]\n")

    try:
        while True:
            sys.stdout.write(",")
            sys.stdout.write(json.dumps(
                [
                    {
                        "full_text" : info.func(),
                        "color"     : info.color
                    } for info in module_infos
                ]
                ))
            sys.stdout.write("\n")
            sys.stdout.flush()

            time.sleep(1)
    except KeyboardInterrupt:
        print("Bye")
    except Exception as e:
        print(e)

    print("]")


if __name__ == '__main__':
    main()
