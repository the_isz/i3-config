#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import os.path
import functools
 

def file_contents(base_path, file_name):
    with open(os.path.join(base_path, file_name), "r") as f:
        return f.read().strip()


class Battery(object):
    """
    Docstring for Battery
    """

    def __init__(self, config={}):
        """
        Constructor

        @param config: A map containing configuration parameters:
            "name" - The name of the directory containing battery information,
                     i.e. the part after "/sys/class/power_supply"
        """
        super(Battery, self).__init__()

        self.battery_name = config.get("name", "BAT0")


    def status(self):
        path_root = "/sys/class/power_supply/{}".format(self.battery_name)

        if not os.path.exists(path_root) or not os.path.isdir(path_root):
            return "N.A."

        fc = functools.partial(file_contents, path_root)

        energy_full_design = int(fc("energy_full_design"))
        energy_now         = int(fc("energy_now"        ))
        power_now          = int(fc("power_now"         ))
        status             =     fc("status"            )

        #print("energy_full_design: {}".format(energy_full_design))
        #print("energy_now        : {}".format(energy_now))
        #print("power_now         : {}".format(power_now))
        #print("status            : {}".format(status))

        if status == "Full":
            return "☇" # ⚡

        # power_now may be 0!?
        if power_now < 1:
            return "?"

        percent = int(round(float(energy_now) / energy_full_design * 100))
        minutes = 0
        symbol = ""

        if status == "Discharging":
            minutes = int(round(float(energy_now) / power_now * 60))
            symbol = "▼"

        if status == "Charging":
            minutes = int(round(float(energy_full_design - energy_now) / power_now * 60))
            symbol ="▲"

        return "{} {}% ({}:{:#02})".format(symbol, percent, minutes / 60, minutes % 60)


if __name__ == '__main__':
    battery = Battery()
    print(battery.status())
