# -*- coding: utf-8 -*-
# vim: set sw=4 et tw=79:

from __future__ import unicode_literals

import os.path
import mailbox
import sys

mail_dir_paths = \
    { "GMX" : "/home/rincewind/Mail/gmx"
    , "GMail" : "/home/rincewind/Mail/google"
    }

subfolders = \
    { "GMX" : [("INBOX", "GMX"), ("Spamverdacht", "Spam")]
    , "GMail" : [("INBOX", "GMail")]
    }

separator = "  "


def mailbox_state(mail_dir_path, subfolders):
    """
    Scans the mailbox for new messages.

    @param mail_dir_path: The directory in which the mailbox resides.

    @param subfolders: An iterable containing 2-tuples of subfolder -> name
                       mappings where subfolder is the folder to check for
                       mails and name is the display name to use.

    @return: A dictionary mapping the name entries from the subfolders
             parameter to the number of new mails in that subfolder.
    """
    state = {}

    def to_maildir_fmt(paths):
        for path in iter(paths):
            yield path.rsplit(":")[0]

    for subfolder, display_name in subfolders:
        path = os.path.join(mail_dir_path, subfolder)
        maildir = mailbox.Maildir(path)
        state[display_name] = 0

        for file in to_maildir_fmt(os.listdir(os.path.join(path, "new"))):
            if file in maildir:
                state[display_name] += 1

    return state


def format_text(state):
    """
    Converts the state of the subfolders to a string.

    @param state: a dictionary as returned by mailbox_state.
    @return: a string representation of the given state.
    """
    items = state.iteritems() if sys.version < '3' else state.items()
    return separator.join(
            "{} {}".format(*item) for item in items)



class Mail(object):
    """
    Docstring for Mail
    """

    def __init__(self, *args, **kwargs):
        """
        """
        super(Mail, self).__init__(*args, **kwargs)


    def status(self):
        return separator.join(
            format_text(
                mailbox_state(
                    mail_dir_paths[account],
                    subfolders[account]
                    )
                ) for account in ["GMX", "GMail"]
            )
