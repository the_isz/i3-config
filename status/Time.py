#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

class Time(object):
    """
    Docstring for Time
    """

    def __init__(self, *args, **kwargs):
        """
        """
        super(Time, self).__init__(*args, **kwargs)

    def status(self):
        return time.strftime("%d.%m. %H:%M")
